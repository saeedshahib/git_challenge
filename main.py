def verify_token_and_add_email(self, trader, token):
    self.try_to_verify += 1
    if self.try_to_verify > TokenVerificationTryLimit:
        self.state = RegistrationSignInOTPState.Expired.value
        self.save()
        return False
    self.save()

    # Expired
    if self.created + tabdeal.settings.REGISTRATION_OTP_EXPIRATION < datetime.now(tz=timezone.utc):
        self.state = RegistrationSignInOTPState.Expired.value
        self.save()
        return False
    elif self.state == RegistrationSignInOTPState.Pending.value and self.token == token:
        try:
            with transaction.atomic():
                trader.email = self.email
                trader.email_verified = True
                trader.save()
                trader.credentials.email = self.email
                trader.credentials.save()
                self.state = RegistrationSignInOTPState.Verified.value
                self.save()
                return True
        except Exception as ve:
            exception(ve, traceback)
            self.state = RegistrationSignInOTPState.Error.value
            self.save()
            return False
    else:
        return False